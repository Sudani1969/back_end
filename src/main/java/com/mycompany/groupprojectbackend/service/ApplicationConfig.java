/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.groupprojectbackend.service;

import java.util.Set;
import javax.ws.rs.core.Application;

/**
 *
 * @author Abbas
 */
@javax.ws.rs.ApplicationPath("webresources")
public class ApplicationConfig extends Application {

    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> resources = new java.util.HashSet<>();
        addRestResourceClasses(resources);
        return resources;
    }

    /**
     * Do not modify addRestResourceClasses() method.
     * It is automatically populated with
     * all resources defined in the project.
     * If required, comment out calling this method in getClasses().
     */
    private void addRestResourceClasses(Set<Class<?>> resources) {
        resources.add(com.mycompany.groupprojectbackend.NewCrossOriginResourceSharingFilter.class);
        resources.add(com.mycompany.groupprojectbackend.service.DriverPropFacadeREST.class);
        resources.add(com.mycompany.groupprojectbackend.service.DriverdescFacadeREST.class);
        resources.add(com.mycompany.groupprojectbackend.service.OutputFacadeREST.class);
        resources.add(com.mycompany.groupprojectbackend.service.SectionFacadeREST.class);
        resources.add(com.mycompany.groupprojectbackend.service.TrackDescFacadeREST.class);
        resources.add(com.mycompany.groupprojectbackend.service.VehicleAccnPropFacadeREST.class);
        resources.add(com.mycompany.groupprojectbackend.service.VehicleDescFacadeREST.class);
        resources.add(com.mycompany.groupprojectbackend.service.VehiclePropFacadeREST.class);
    }
    
}
