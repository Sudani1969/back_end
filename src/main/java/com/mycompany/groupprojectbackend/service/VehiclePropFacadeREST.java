/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.groupprojectbackend.service;

import com.mycompany.groupprojectbackend.VehicleProp;
import com.mycompany.groupprojectbackend.VehiclePropPK;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.PathSegment;

/**
 *
 * @author Abbas
 */
@Stateless
@Path("vehicleprops")
public class VehiclePropFacadeREST extends AbstractFacade<VehicleProp> {
    @PersistenceContext(unitName = "com.mycompany_groupProjectBackend_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    private VehiclePropPK getPrimaryKey(PathSegment pathSegment) {
        /*
         * pathSemgent represents a URI path segment and any associated matrix parameters.
         * URI path part is supposed to be in form of 'somePath;vehicleID=vehicleIDValue;gear=gearValue;rpm=rpmValue'.
         * Here 'somePath' is a result of getPath() method invocation and
         * it is ignored in the following code.
         * Matrix parameters are used as field names to build a primary key instance.
         */
        com.mycompany.groupprojectbackend.VehiclePropPK key = new com.mycompany.groupprojectbackend.VehiclePropPK();
        javax.ws.rs.core.MultivaluedMap<String, String> map = pathSegment.getMatrixParameters();
        java.util.List<String> vehicleID = map.get("vehicleID");
        if (vehicleID != null && !vehicleID.isEmpty()) {
            key.setVehicleID(new java.lang.Integer(vehicleID.get(0)));
        }
        java.util.List<String> gear = map.get("gear");
        if (gear != null && !gear.isEmpty()) {
            key.setGear(new java.lang.Integer(gear.get(0)));
        }
        java.util.List<String> rpm = map.get("rpm");
        if (rpm != null && !rpm.isEmpty()) {
            key.setRpm(new java.lang.Float(rpm.get(0)));
        }
        return key;
    }

    public VehiclePropFacadeREST() {
        super(VehicleProp.class);
    }

    @POST
    @Override
    @Consumes({"application/xml", "application/json"})
    public void create(VehicleProp entity) {
        super.create(entity);
    }

    @PUT
    @Path("{id}")
    @Consumes({"application/xml", "application/json"})
    public void edit(@PathParam("id") PathSegment id, VehicleProp entity) {
        super.edit(entity);
    }

    @DELETE
    @Path("{id}")
    public void remove(@PathParam("id") PathSegment id) {
        com.mycompany.groupprojectbackend.VehiclePropPK key = getPrimaryKey(id);
        super.remove(super.find(key));
    }

    @GET
    @Path("{id}")
    @Produces({"application/xml", "application/json"})
    public VehicleProp find(@PathParam("id") PathSegment id) {
        com.mycompany.groupprojectbackend.VehiclePropPK key = getPrimaryKey(id);
        return super.find(key);
    }

    @GET
    @Override
    @Produces({"application/xml", "application/json"})
    public List<VehicleProp> findAll() {
        return super.findAll();
    }

    @GET
    @Path("{from}/{to}")
    @Produces({"application/xml", "application/json"})
    public List<VehicleProp> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        return super.findRange(new int[]{from, to});
    }

    @GET
    @Path("count")
    @Produces("text/plain")
    public String countREST() {
        return String.valueOf(super.count());
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
}
