/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.groupprojectbackend;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Abbas
 */
@Entity
@Table(name = "driverdesc")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Driverdesc.findAll", query = "SELECT d FROM Driverdesc d"),
    @NamedQuery(name = "Driverdesc.findByDriverID", query = "SELECT d FROM Driverdesc d WHERE d.driverID = :driverID"),
    @NamedQuery(name = "Driverdesc.findByDriverDesc", query = "SELECT d FROM Driverdesc d WHERE d.driverDesc = :driverDesc")})
public class Driverdesc implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "DriverID")
    private Integer driverID;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "DriverDesc")
    private String driverDesc;

    public Driverdesc() {
    }

    public Driverdesc(Integer driverID) {
        this.driverID = driverID;
    }

    public Driverdesc(Integer driverID, String driverDesc) {
        this.driverID = driverID;
        this.driverDesc = driverDesc;
    }

    public Integer getDriverID() {
        return driverID;
    }

    public void setDriverID(Integer driverID) {
        this.driverID = driverID;
    }

    public String getDriverDesc() {
        return driverDesc;
    }

    public void setDriverDesc(String driverDesc) {
        this.driverDesc = driverDesc;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (driverID != null ? driverID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Driverdesc)) {
            return false;
        }
        Driverdesc other = (Driverdesc) object;
        if ((this.driverID == null && other.driverID != null) || (this.driverID != null && !this.driverID.equals(other.driverID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mycompany.groupprojectbackend.Driverdesc[ driverID=" + driverID + " ]";
    }
    
}
