/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.groupprojectbackend;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Abbas
 */
@Embeddable
public class OutputPK implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Column(name = "OutputID")
    private int outputID;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Time")
    private float time;

    public OutputPK() {
    }

    public OutputPK(int outputID, float time) {
        this.outputID = outputID;
        this.time = time;
    }

    public int getOutputID() {
        return outputID;
    }

    public void setOutputID(int outputID) {
        this.outputID = outputID;
    }

    public float getTime() {
        return time;
    }

    public void setTime(float time) {
        this.time = time;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) outputID;
        hash += (int) time;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof OutputPK)) {
            return false;
        }
        OutputPK other = (OutputPK) object;
        if (this.outputID != other.outputID) {
            return false;
        }
        if (this.time != other.time) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mycompany.groupprojectbackend.OutputPK[ outputID=" + outputID + ", time=" + time + " ]";
    }
    
}
