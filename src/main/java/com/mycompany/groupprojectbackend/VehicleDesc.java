/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.groupprojectbackend;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Abbas
 */
@Entity
@Table(name = "VehicleDesc")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VehicleDesc.findAll", query = "SELECT v FROM VehicleDesc v"),
    @NamedQuery(name = "VehicleDesc.findByVehicleDesc", query = "SELECT v FROM VehicleDesc v WHERE v.vehicleDesc = :vehicleDesc"),
    @NamedQuery(name = "VehicleDesc.findByVehicleID", query = "SELECT v FROM VehicleDesc v WHERE v.vehicleID = :vehicleID")})
public class VehicleDesc implements Serializable {
    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "VehicleDesc")
    private String vehicleDesc;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "VehicleID")
    private Integer vehicleID;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "vehicleDesc")
    private Collection<VehicleProp> vehiclePropCollection;

    public VehicleDesc() {
    }

    public VehicleDesc(Integer vehicleID) {
        this.vehicleID = vehicleID;
    }

    public VehicleDesc(Integer vehicleID, String vehicleDesc) {
        this.vehicleID = vehicleID;
        this.vehicleDesc = vehicleDesc;
    }

    public String getVehicleDesc() {
        return vehicleDesc;
    }

    public void setVehicleDesc(String vehicleDesc) {
        this.vehicleDesc = vehicleDesc;
    }

    public Integer getVehicleID() {
        return vehicleID;
    }

    public void setVehicleID(Integer vehicleID) {
        this.vehicleID = vehicleID;
    }

    @XmlTransient
    public Collection<VehicleProp> getVehiclePropCollection() {
        return vehiclePropCollection;
    }

    public void setVehiclePropCollection(Collection<VehicleProp> vehiclePropCollection) {
        this.vehiclePropCollection = vehiclePropCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (vehicleID != null ? vehicleID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof VehicleDesc)) {
            return false;
        }
        VehicleDesc other = (VehicleDesc) object;
        if ((this.vehicleID == null && other.vehicleID != null) || (this.vehicleID != null && !this.vehicleID.equals(other.vehicleID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mycompany.groupprojectbackend.VehicleDesc[ vehicleID=" + vehicleID + " ]";
    }
    
}
