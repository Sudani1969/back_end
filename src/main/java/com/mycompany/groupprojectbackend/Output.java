/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.groupprojectbackend;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Abbas
 */
@Entity
@Table(name = "Output")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Output.findAll", query = "SELECT o FROM Output o"),
    @NamedQuery(name = "Output.findByOutputID", query = "SELECT o FROM Output o WHERE o.outputPK.outputID = :outputID"),
    @NamedQuery(name = "Output.findByXPosn", query = "SELECT o FROM Output o WHERE o.xPosn = :xPosn"),
    @NamedQuery(name = "Output.findByYPosn", query = "SELECT o FROM Output o WHERE o.yPosn = :yPosn"),
    @NamedQuery(name = "Output.findByTime", query = "SELECT o FROM Output o WHERE o.outputPK.time = :time"),
    @NamedQuery(name = "Output.findBySpeed", query = "SELECT o FROM Output o WHERE o.speed = :speed"),
    @NamedQuery(name = "Output.findByRpm", query = "SELECT o FROM Output o WHERE o.rpm = :rpm"),
    @NamedQuery(name = "Output.findByGear", query = "SELECT o FROM Output o WHERE o.gear = :gear"),
    @NamedQuery(name = "Output.findByFuelUse", query = "SELECT o FROM Output o WHERE o.fuelUse = :fuelUse"),
    @NamedQuery(name = "Output.findByEmissions", query = "SELECT o FROM Output o WHERE o.emissions = :emissions")})
public class Output implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected OutputPK outputPK;
    @Basic(optional = false)
    @NotNull
    @Column(name = "XPosn")
    private float xPosn;
    @Basic(optional = false)
    @NotNull
    @Column(name = "YPosn")
    private float yPosn;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Speed")
    private float speed;
    @Basic(optional = false)
    @NotNull
    @Column(name = "RPM")
    private float rpm;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Gear")
    private int gear;
    @Basic(optional = false)
    @NotNull
    @Column(name = "FuelUse")
    private float fuelUse;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Emissions")
    private float emissions;

    public Output() {
    }

    public Output(OutputPK outputPK) {
        this.outputPK = outputPK;
    }

    public Output(OutputPK outputPK, float xPosn, float yPosn, float speed, float rpm, int gear, float fuelUse, float emissions) {
        this.outputPK = outputPK;
        this.xPosn = xPosn;
        this.yPosn = yPosn;
        this.speed = speed;
        this.rpm = rpm;
        this.gear = gear;
        this.fuelUse = fuelUse;
        this.emissions = emissions;
    }

    public Output(int outputID, float time) {
        this.outputPK = new OutputPK(outputID, time);
    }

    public OutputPK getOutputPK() {
        return outputPK;
    }

    public void setOutputPK(OutputPK outputPK) {
        this.outputPK = outputPK;
    }

    public float getXPosn() {
        return xPosn;
    }

    public void setXPosn(float xPosn) {
        this.xPosn = xPosn;
    }

    public float getYPosn() {
        return yPosn;
    }

    public void setYPosn(float yPosn) {
        this.yPosn = yPosn;
    }

    public float getSpeed() {
        return speed;
    }

    public void setSpeed(float speed) {
        this.speed = speed;
    }

    public float getRpm() {
        return rpm;
    }

    public void setRpm(float rpm) {
        this.rpm = rpm;
    }

    public int getGear() {
        return gear;
    }

    public void setGear(int gear) {
        this.gear = gear;
    }

    public float getFuelUse() {
        return fuelUse;
    }

    public void setFuelUse(float fuelUse) {
        this.fuelUse = fuelUse;
    }

    public float getEmissions() {
        return emissions;
    }

    public void setEmissions(float emissions) {
        this.emissions = emissions;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (outputPK != null ? outputPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Output)) {
            return false;
        }
        Output other = (Output) object;
        if ((this.outputPK == null && other.outputPK != null) || (this.outputPK != null && !this.outputPK.equals(other.outputPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mycompany.groupprojectbackend.Output[ outputPK=" + outputPK + " ]";
    }
    
}
