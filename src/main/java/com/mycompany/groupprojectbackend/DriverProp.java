/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.groupprojectbackend;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Abbas
 */
@Entity
@Table(name = "DriverProp")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DriverProp.findAll", query = "SELECT d FROM DriverProp d"),
    @NamedQuery(name = "DriverProp.findByDriverID", query = "SELECT d FROM DriverProp d WHERE d.driverID = :driverID"),
    @NamedQuery(name = "DriverProp.findByGear", query = "SELECT d FROM DriverProp d WHERE d.gear = :gear"),
    @NamedQuery(name = "DriverProp.findByChangeUpRPM", query = "SELECT d FROM DriverProp d WHERE d.changeUpRPM = :changeUpRPM"),
    @NamedQuery(name = "DriverProp.findByChangeDownRPM", query = "SELECT d FROM DriverProp d WHERE d.changeDownRPM = :changeDownRPM"),
    @NamedQuery(name = "DriverProp.findByPreferredMaxRPM", query = "SELECT d FROM DriverProp d WHERE d.preferredMaxRPM = :preferredMaxRPM"),
    @NamedQuery(name = "DriverProp.findByPreferredMinRPM", query = "SELECT d FROM DriverProp d WHERE d.preferredMinRPM = :preferredMinRPM"),
    @NamedQuery(name = "DriverProp.findByDecisionTime", query = "SELECT d FROM DriverProp d WHERE d.decisionTime = :decisionTime"),
    @NamedQuery(name = "DriverProp.findByMaxAccn", query = "SELECT d FROM DriverProp d WHERE d.maxAccn = :maxAccn"),
    @NamedQuery(name = "DriverProp.findByMaxBreaking", query = "SELECT d FROM DriverProp d WHERE d.maxBreaking = :maxBreaking"),
    @NamedQuery(name = "DriverProp.findByMaxStraightSpeed", query = "SELECT d FROM DriverProp d WHERE d.maxStraightSpeed = :maxStraightSpeed"),
    @NamedQuery(name = "DriverProp.findByMaxCornerSpeed", query = "SELECT d FROM DriverProp d WHERE d.maxCornerSpeed = :maxCornerSpeed")})
public class DriverProp implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "DriverID")
    private Integer driverID;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Gear")
    private int gear;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ChangeUpRPM")
    private float changeUpRPM;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ChangeDownRPM")
    private float changeDownRPM;
    @Basic(optional = false)
    @NotNull
    @Column(name = "PreferredMaxRPM")
    private float preferredMaxRPM;
    @Basic(optional = false)
    @NotNull
    @Column(name = "PreferredMinRPM")
    private float preferredMinRPM;
    @Basic(optional = false)
    @NotNull
    @Column(name = "DecisionTime")
    private float decisionTime;
    @Basic(optional = false)
    @NotNull
    @Column(name = "MaxAccn")
    private float maxAccn;
    @Basic(optional = false)
    @NotNull
    @Column(name = "MaxBreaking")
    private float maxBreaking;
    @Basic(optional = false)
    @NotNull
    @Column(name = "MaxStraightSpeed")
    private float maxStraightSpeed;
    @Basic(optional = false)
    @NotNull
    @Column(name = "MaxCornerSpeed")
    private float maxCornerSpeed;

    public DriverProp() {
    }

    public DriverProp(Integer driverID) {
        this.driverID = driverID;
    }

    public DriverProp(Integer driverID, int gear, float changeUpRPM, float changeDownRPM, float preferredMaxRPM, float preferredMinRPM, float decisionTime, float maxAccn, float maxBreaking, float maxStraightSpeed, float maxCornerSpeed) {
        this.driverID = driverID;
        this.gear = gear;
        this.changeUpRPM = changeUpRPM;
        this.changeDownRPM = changeDownRPM;
        this.preferredMaxRPM = preferredMaxRPM;
        this.preferredMinRPM = preferredMinRPM;
        this.decisionTime = decisionTime;
        this.maxAccn = maxAccn;
        this.maxBreaking = maxBreaking;
        this.maxStraightSpeed = maxStraightSpeed;
        this.maxCornerSpeed = maxCornerSpeed;
    }

    public Integer getDriverID() {
        return driverID;
    }

    public void setDriverID(Integer driverID) {
        this.driverID = driverID;
    }

    public int getGear() {
        return gear;
    }

    public void setGear(int gear) {
        this.gear = gear;
    }

    public float getChangeUpRPM() {
        return changeUpRPM;
    }

    public void setChangeUpRPM(float changeUpRPM) {
        this.changeUpRPM = changeUpRPM;
    }

    public float getChangeDownRPM() {
        return changeDownRPM;
    }

    public void setChangeDownRPM(float changeDownRPM) {
        this.changeDownRPM = changeDownRPM;
    }

    public float getPreferredMaxRPM() {
        return preferredMaxRPM;
    }

    public void setPreferredMaxRPM(float preferredMaxRPM) {
        this.preferredMaxRPM = preferredMaxRPM;
    }

    public float getPreferredMinRPM() {
        return preferredMinRPM;
    }

    public void setPreferredMinRPM(float preferredMinRPM) {
        this.preferredMinRPM = preferredMinRPM;
    }

    public float getDecisionTime() {
        return decisionTime;
    }

    public void setDecisionTime(float decisionTime) {
        this.decisionTime = decisionTime;
    }

    public float getMaxAccn() {
        return maxAccn;
    }

    public void setMaxAccn(float maxAccn) {
        this.maxAccn = maxAccn;
    }

    public float getMaxBreaking() {
        return maxBreaking;
    }

    public void setMaxBreaking(float maxBreaking) {
        this.maxBreaking = maxBreaking;
    }

    public float getMaxStraightSpeed() {
        return maxStraightSpeed;
    }

    public void setMaxStraightSpeed(float maxStraightSpeed) {
        this.maxStraightSpeed = maxStraightSpeed;
    }

    public float getMaxCornerSpeed() {
        return maxCornerSpeed;
    }

    public void setMaxCornerSpeed(float maxCornerSpeed) {
        this.maxCornerSpeed = maxCornerSpeed;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (driverID != null ? driverID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DriverProp)) {
            return false;
        }
        DriverProp other = (DriverProp) object;
        return !((this.driverID == null && other.driverID != null) || (this.driverID != null && !this.driverID.equals(other.driverID)));
    }

    @Override
    public String toString() {
        return "com.mycompany.groupprojectbackend.DriverProp[ driverID=" + driverID + " ]";
    }
    
}
