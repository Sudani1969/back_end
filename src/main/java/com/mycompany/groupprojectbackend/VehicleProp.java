/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.groupprojectbackend;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Abbas
 */
@Entity
@Table(name = "VehicleProp")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VehicleProp.findAll", query = "SELECT v FROM VehicleProp v"),
    @NamedQuery(name = "VehicleProp.findByVehicleID", query = "SELECT v FROM VehicleProp v WHERE v.vehiclePropPK.vehicleID = :vehicleID"),
    @NamedQuery(name = "VehicleProp.findByGear", query = "SELECT v FROM VehicleProp v WHERE v.vehiclePropPK.gear = :gear"),
    @NamedQuery(name = "VehicleProp.findByRpm", query = "SELECT v FROM VehicleProp v WHERE v.vehiclePropPK.rpm = :rpm"),
    @NamedQuery(name = "VehicleProp.findByFuelUse", query = "SELECT v FROM VehicleProp v WHERE v.fuelUse = :fuelUse"),
    @NamedQuery(name = "VehicleProp.findByMaxAccn", query = "SELECT v FROM VehicleProp v WHERE v.maxAccn = :maxAccn"),
    @NamedQuery(name = "VehicleProp.findByMaxBreak", query = "SELECT v FROM VehicleProp v WHERE v.maxBreak = :maxBreak"),
    @NamedQuery(name = "VehicleProp.findByVehicleSpeed", query = "SELECT v FROM VehicleProp v WHERE v.vehicleSpeed = :vehicleSpeed")})
public class VehicleProp implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected VehiclePropPK vehiclePropPK;
    @Basic(optional = false)
    @NotNull
    @Column(name = "FuelUse")
    private float fuelUse;
    @Basic(optional = false)
    @NotNull
    @Column(name = "MaxAccn")
    private float maxAccn;
    @Basic(optional = false)
    @NotNull
    @Column(name = "MaxBreak")
    private float maxBreak;
    @Basic(optional = false)
    @NotNull
    @Column(name = "VehicleSpeed")
    private float vehicleSpeed;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "vehicleProp")
    private Collection<VehicleAccnProp> vehicleAccnPropCollection;
    @JoinColumn(name = "VehicleID", referencedColumnName = "VehicleID", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private VehicleDesc vehicleDesc;

    public VehicleProp() {
    }

    public VehicleProp(VehiclePropPK vehiclePropPK) {
        this.vehiclePropPK = vehiclePropPK;
    }

    public VehicleProp(VehiclePropPK vehiclePropPK, float fuelUse, float maxAccn, float maxBreak, float vehicleSpeed) {
        this.vehiclePropPK = vehiclePropPK;
        this.fuelUse = fuelUse;
        this.maxAccn = maxAccn;
        this.maxBreak = maxBreak;
        this.vehicleSpeed = vehicleSpeed;
    }

    public VehicleProp(int vehicleID, int gear, float rpm) {
        this.vehiclePropPK = new VehiclePropPK(vehicleID, gear, rpm);
    }

    public VehiclePropPK getVehiclePropPK() {
        return vehiclePropPK;
    }

    public void setVehiclePropPK(VehiclePropPK vehiclePropPK) {
        this.vehiclePropPK = vehiclePropPK;
    }

    public float getFuelUse() {
        return fuelUse;
    }

    public void setFuelUse(float fuelUse) {
        this.fuelUse = fuelUse;
    }

    public float getMaxAccn() {
        return maxAccn;
    }

    public void setMaxAccn(float maxAccn) {
        this.maxAccn = maxAccn;
    }

    public float getMaxBreak() {
        return maxBreak;
    }

    public void setMaxBreak(float maxBreak) {
        this.maxBreak = maxBreak;
    }

    public float getVehicleSpeed() {
        return vehicleSpeed;
    }

    public void setVehicleSpeed(float vehicleSpeed) {
        this.vehicleSpeed = vehicleSpeed;
    }

    @XmlTransient
    public Collection<VehicleAccnProp> getVehicleAccnPropCollection() {
        return vehicleAccnPropCollection;
    }

    public void setVehicleAccnPropCollection(Collection<VehicleAccnProp> vehicleAccnPropCollection) {
        this.vehicleAccnPropCollection = vehicleAccnPropCollection;
    }

    public VehicleDesc getVehicleDesc() {
        return vehicleDesc;
    }

    public void setVehicleDesc(VehicleDesc vehicleDesc) {
        this.vehicleDesc = vehicleDesc;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (vehiclePropPK != null ? vehiclePropPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof VehicleProp)) {
            return false;
        }
        VehicleProp other = (VehicleProp) object;
        if ((this.vehiclePropPK == null && other.vehiclePropPK != null) || (this.vehiclePropPK != null && !this.vehiclePropPK.equals(other.vehiclePropPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mycompany.groupprojectbackend.VehicleProp[ vehiclePropPK=" + vehiclePropPK + " ]";
    }
    
}
