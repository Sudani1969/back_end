/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.groupprojectbackend;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Abbas
 */
@Entity
@Table(name = "Section")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Section.findAll", query = "SELECT s FROM Section s"),
    @NamedQuery(name = "Section.findBySecnID", query = "SELECT s FROM Section s WHERE s.secnID = :secnID"),
    @NamedQuery(name = "Section.findByDirection", query = "SELECT s FROM Section s WHERE s.direction = :direction"),
    @NamedQuery(name = "Section.findByRadius", query = "SELECT s FROM Section s WHERE s.radius = :radius"),
    @NamedQuery(name = "Section.findBySecnLength", query = "SELECT s FROM Section s WHERE s.secnLength = :secnLength"),
    @NamedQuery(name = "Section.findBySecnMaxSpeed", query = "SELECT s FROM Section s WHERE s.secnMaxSpeed = :secnMaxSpeed")})
public class Section implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "SecnID")
    private Integer secnID;
    @Column(name = "Direction")
    private Serializable direction;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "Radius")
    private Float radius;
    @Basic(optional = false)
    @NotNull
    @Column(name = "SecnLength")
    private float secnLength;
    @Basic(optional = false)
    @NotNull
    @Column(name = "SecnMaxSpeed")
    private float secnMaxSpeed;
    @JoinColumn(name = "TrackID", referencedColumnName = "TrackID")
    @ManyToOne(optional = false)
    private TrackDesc trackID;

    public Section() {
    }

    public Section(Integer secnID) {
        this.secnID = secnID;
    }

    public Section(Integer secnID, float secnLength, float secnMaxSpeed) {
        this.secnID = secnID;
        this.secnLength = secnLength;
        this.secnMaxSpeed = secnMaxSpeed;
    }

    public Integer getSecnID() {
        return secnID;
    }

    public void setSecnID(Integer secnID) {
        this.secnID = secnID;
    }

    public Serializable getDirection() {
        return direction;
    }

    public void setDirection(Serializable direction) {
        this.direction = direction;
    }

    public Float getRadius() {
        return radius;
    }

    public void setRadius(Float radius) {
        this.radius = radius;
    }

    public float getSecnLength() {
        return secnLength;
    }

    public void setSecnLength(float secnLength) {
        this.secnLength = secnLength;
    }

    public float getSecnMaxSpeed() {
        return secnMaxSpeed;
    }

    public void setSecnMaxSpeed(float secnMaxSpeed) {
        this.secnMaxSpeed = secnMaxSpeed;
    }

    public TrackDesc getTrackID() {
        return trackID;
    }

    public void setTrackID(TrackDesc trackID) {
        this.trackID = trackID;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (secnID != null ? secnID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Section)) {
            return false;
        }
        Section other = (Section) object;
        if ((this.secnID == null && other.secnID != null) || (this.secnID != null && !this.secnID.equals(other.secnID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mycompany.groupprojectbackend.Section[ secnID=" + secnID + " ]";
    }
    
}
