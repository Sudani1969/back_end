/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.groupprojectbackend;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Abbas
 */
@Entity
@Table(name = "TrackDesc")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TrackDesc.findAll", query = "SELECT t FROM TrackDesc t"),
    @NamedQuery(name = "TrackDesc.findByTrackID", query = "SELECT t FROM TrackDesc t WHERE t.trackID = :trackID"),
    @NamedQuery(name = "TrackDesc.findByTrackDesc", query = "SELECT t FROM TrackDesc t WHERE t.trackDesc = :trackDesc"),
    @NamedQuery(name = "TrackDesc.findByStartDirection", query = "SELECT t FROM TrackDesc t WHERE t.startDirection = :startDirection")})
public class TrackDesc implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "TrackID")
    private Integer trackID;
    @Size(max = 2147483647)
    @Column(name = "TrackDesc")
    private String trackDesc;
    @Basic(optional = false)
    @NotNull
    @Column(name = "StartDirection")
    private float startDirection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "trackID")
    private Collection<Section> sectionCollection;

    public TrackDesc() {
    }

    public TrackDesc(Integer trackID) {
        this.trackID = trackID;
    }

    public TrackDesc(Integer trackID, float startDirection) {
        this.trackID = trackID;
        this.startDirection = startDirection;
    }

    public Integer getTrackID() {
        return trackID;
    }

    public void setTrackID(Integer trackID) {
        this.trackID = trackID;
    }

    public String getTrackDesc() {
        return trackDesc;
    }

    public void setTrackDesc(String trackDesc) {
        this.trackDesc = trackDesc;
    }

    public float getStartDirection() {
        return startDirection;
    }

    public void setStartDirection(float startDirection) {
        this.startDirection = startDirection;
    }

    @XmlTransient
    public Collection<Section> getSectionCollection() {
        return sectionCollection;
    }

    public void setSectionCollection(Collection<Section> sectionCollection) {
        this.sectionCollection = sectionCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (trackID != null ? trackID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TrackDesc)) {
            return false;
        }
        TrackDesc other = (TrackDesc) object;
        if ((this.trackID == null && other.trackID != null) || (this.trackID != null && !this.trackID.equals(other.trackID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mycompany.groupprojectbackend.TrackDesc[ trackID=" + trackID + " ]";
    }
    
}
