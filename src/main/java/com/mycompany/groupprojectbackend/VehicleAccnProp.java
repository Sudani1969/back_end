/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.groupprojectbackend;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Abbas
 */
@Entity
@Table(name = "VehicleAccnProp")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VehicleAccnProp.findAll", query = "SELECT v FROM VehicleAccnProp v"),
    @NamedQuery(name = "VehicleAccnProp.findByVehicleID", query = "SELECT v FROM VehicleAccnProp v WHERE v.vehicleAccnPropPK.vehicleID = :vehicleID"),
    @NamedQuery(name = "VehicleAccnProp.findByGear", query = "SELECT v FROM VehicleAccnProp v WHERE v.vehicleAccnPropPK.gear = :gear"),
    @NamedQuery(name = "VehicleAccnProp.findByRpm", query = "SELECT v FROM VehicleAccnProp v WHERE v.vehicleAccnPropPK.rpm = :rpm"),
    @NamedQuery(name = "VehicleAccnProp.findByAccn", query = "SELECT v FROM VehicleAccnProp v WHERE v.vehicleAccnPropPK.accn = :accn"),
    @NamedQuery(name = "VehicleAccnProp.findByFuelUse", query = "SELECT v FROM VehicleAccnProp v WHERE v.fuelUse = :fuelUse")})
public class VehicleAccnProp implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected VehicleAccnPropPK vehicleAccnPropPK;
    @Basic(optional = false)
    @NotNull
    @Column(name = "FuelUse")
    private float fuelUse;
    @JoinColumns({
        @JoinColumn(name = "VehicleID", referencedColumnName = "VehicleID", insertable = false, updatable = false),
        @JoinColumn(name = "Gear", referencedColumnName = "Gear", insertable = false, updatable = false),
        @JoinColumn(name = "RPM", referencedColumnName = "RPM", insertable = false, updatable = false)})
    @ManyToOne(optional = false)
    private VehicleProp vehicleProp;

    public VehicleAccnProp() {
    }

    public VehicleAccnProp(VehicleAccnPropPK vehicleAccnPropPK) {
        this.vehicleAccnPropPK = vehicleAccnPropPK;
    }

    public VehicleAccnProp(VehicleAccnPropPK vehicleAccnPropPK, float fuelUse) {
        this.vehicleAccnPropPK = vehicleAccnPropPK;
        this.fuelUse = fuelUse;
    }

    public VehicleAccnProp(int vehicleID, int gear, float rpm, float accn) {
        this.vehicleAccnPropPK = new VehicleAccnPropPK(vehicleID, gear, rpm, accn);
    }

    public VehicleAccnPropPK getVehicleAccnPropPK() {
        return vehicleAccnPropPK;
    }

    public void setVehicleAccnPropPK(VehicleAccnPropPK vehicleAccnPropPK) {
        this.vehicleAccnPropPK = vehicleAccnPropPK;
    }

    public float getFuelUse() {
        return fuelUse;
    }

    public void setFuelUse(float fuelUse) {
        this.fuelUse = fuelUse;
    }

    public VehicleProp getVehicleProp() {
        return vehicleProp;
    }

    public void setVehicleProp(VehicleProp vehicleProp) {
        this.vehicleProp = vehicleProp;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (vehicleAccnPropPK != null ? vehicleAccnPropPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof VehicleAccnProp)) {
            return false;
        }
        VehicleAccnProp other = (VehicleAccnProp) object;
        if ((this.vehicleAccnPropPK == null && other.vehicleAccnPropPK != null) || (this.vehicleAccnPropPK != null && !this.vehicleAccnPropPK.equals(other.vehicleAccnPropPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mycompany.groupprojectbackend.VehicleAccnProp[ vehicleAccnPropPK=" + vehicleAccnPropPK + " ]";
    }
    
}
