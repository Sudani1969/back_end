/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.groupprojectbackend;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Abbas
 */
@Embeddable
public class VehicleAccnPropPK implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Column(name = "VehicleID")
    private int vehicleID;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Gear")
    private int gear;
    @Basic(optional = false)
    @NotNull
    @Column(name = "RPM")
    private float rpm;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Accn")
    private float accn;

    public VehicleAccnPropPK() {
    }

    public VehicleAccnPropPK(int vehicleID, int gear, float rpm, float accn) {
        this.vehicleID = vehicleID;
        this.gear = gear;
        this.rpm = rpm;
        this.accn = accn;
    }

    public int getVehicleID() {
        return vehicleID;
    }

    public void setVehicleID(int vehicleID) {
        this.vehicleID = vehicleID;
    }

    public int getGear() {
        return gear;
    }

    public void setGear(int gear) {
        this.gear = gear;
    }

    public float getRpm() {
        return rpm;
    }

    public void setRpm(float rpm) {
        this.rpm = rpm;
    }

    public float getAccn() {
        return accn;
    }

    public void setAccn(float accn) {
        this.accn = accn;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) vehicleID;
        hash += (int) gear;
        hash += (int) rpm;
        hash += (int) accn;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof VehicleAccnPropPK)) {
            return false;
        }
        VehicleAccnPropPK other = (VehicleAccnPropPK) object;
        if (this.vehicleID != other.vehicleID) {
            return false;
        }
        if (this.gear != other.gear) {
            return false;
        }
        if (this.rpm != other.rpm) {
            return false;
        }
        if (this.accn != other.accn) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mycompany.groupprojectbackend.VehicleAccnPropPK[ vehicleID=" + vehicleID + ", gear=" + gear + ", rpm=" + rpm + ", accn=" + accn + " ]";
    }
    
}
